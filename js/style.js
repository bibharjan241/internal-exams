function validate() {
  let fname = document.getElementById("fname").value;
  if (fname == "") {
    document.getElementById("vfname").innerHTML =
      "First Name must not be empty";
  } else {
    document.getElementById("vfname").innerHTML = " ";
  }
  let lname = document.getElementById("lname").value;
  if (lname == "") {
    document.getElementById("vlname").innerHTML =
      "Last Name must not be empty";
  } else {
    document.getElementById("vlname").innerHTML = " ";
  }
  let key = document.getElementById("key").value;
  if (key == "") {
    document.getElementById("vkey").innerHTML =
      "Password must not be empty";
  } else {
    document.getElementById("vkey").innerHTML = " ";
  }
  if (key == "") {
    document.getElementById("key").innerHTML =
      "password must not be empty."
  }
  else {
    if (key.length < 8) {
      document.getElementById("vkey").innerHTML =
        "password must be greater than 8 characters."
    } else {
      document.getElementById('vkey').innerHTML = " "
    }
  }
  let conkey = document.getElementById("conkey").value;
  if (conkey != key) {
    document.getElementById("vconkey").innerHTML =
      "Password does not match";
  } else {
    document.getElementById("vconkey").innerHTML = " ";
  }
  if (conkey == "") {
    document.getElementById("vconkey").innerHTML =
      "Password does not match."
  } else {
    document.getElementById("vconkey").innerHTML = " ";
  }
  let pimage = document.getElementById("pimage").value;
  if (pimage.length == 0) {
    document.getElementById("vpimage").innerHTML =
      "Please select a picture "
  } else {
    document.getElementById("vpimage").innerHTML = " "
  }
  let email = document.getElementById("email").value;
  if (email == "") {
    document.getElementById("vemail").innerHTML =
      "Enter your email. "
  } else {
    if (!email.includes("@") || !email.includes(".com")) {
      document.getElementById("vemail").innerHTML =
        "Enter a correct Email."
    } else {
      document.getElementById("vemail").innerHTML = " "
    }

  }
}
